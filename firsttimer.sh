#!/bin/bash

# Start i2p daemon
i2pd --daemon
echo "Give i2pd some time to start. sleep 10 seconds"
sleep 10

# Create a tunnels.conf file with eepsite configuration for wot-server
echo "creating tunnels.conf for wot-server"
cat <<EOT >> /etc/i2pd/tunnels.conf
[wot-server]
type = http
host = 127.0.0.1
port = 8080
keys = wot-server.dat
EOT

# Add a new tunnel configuration for wot-pool
echo "adding tunnel configuration for wot-pool"
cat <<EOT >> /etc/i2pd/tunnels.conf
[wot-pool]
type = server
host = 127.0.0.1
port = 3333
keys = wot-pool.dat
EOT

# Add SOCKS proxy configuration
echo "adding SOCKS proxy configuration"
cat <<EOT >> /etc/i2pd/tunnels.conf
[SOCKS]
type = socks
address = 127.0.0.1
port = 4447
keys = socks-keys.dat
EOT

# Restart the i2pd service to apply the new configuration
kill $(pgrep i2pd)
sleep 10
i2pd --daemon --service
echo "Give i2pd some time to restart. sleep 10 seconds"
sleep 10

# Download the source code of the tunnels page
curl -s "http://127.0.0.1:7070/?page=i2p_tunnels" > /app/i2p_tunnels.html

# Extract the string for wot-server
result_server=$(grep -oP '(?<=wot-server</a> &#8658; )[a-z0-9]{52}\.b32\.i2p' /app/i2p_tunnels.html)
echo $result_server > /app/wot_i2p_address.txt
echo "Generated I2PID for wot-server: $result_server"

# Extract the string for wot-pool
result_pool=$(grep -oP '(?<=wot-pool</a> &#8658; )[a-z0-9]{52}\.b32\.i2p' /app/i2p_tunnels.html)
echo $result_pool > /app/wot_pool_address.txt
echo "Generated I2PID for wot-pool: $result_pool"

# Clean up
rm /app/i2p_tunnels.html

# Clone the irw-client repository
git clone https://gitlab.com/irate_wot/irw-client.git /app/irw-client

# Clone the irw-server repository
git clone https://gitlab.com/irate_wot/irw-server.git /app/irw-server

# Generate a secure key for Django
SECRET_KEY=$(openssl rand -base64 48)

# Create the .env file if it doesn't exist
touch /app/irw-server/irate_wot/.env

# Check if the SECRET_KEY line exists in the .env file
if grep -q "^SECRET_KEY=" /app/irw-server/irate_wot/.env; then
    # If the line exists, update it with the new key
    sed -i "s/^SECRET_KEY=.*/SECRET_KEY=$SECRET_KEY/" /app/irw-server/irate_wot/.env
else
    # If the line doesn't exist, append it to the file
    echo "SECRET_KEY=$SECRET_KEY" >> /app/irw-server/irate_wot/.env
fi

# Print the generated key
echo "Generated SECRET_KEY: $SECRET_KEY"
sleep 20
