#!/bin/bash

repos=(
    "/app/irw-server"
    "/app/irw-client"
    "/app/irw-s-docker"
    # Add more repos as needed
)

for repo in "${repos[@]}"
do
    if [ -d "$repo" ]; then
        echo "Checking for updates in $repo"
        cd $repo
        git fetch origin
        LOCAL=$(git rev-parse @)
        REMOTE=$(git rev-parse @{u})
        BASE=$(git merge-base @ @{u})
        if [ $LOCAL = $REMOTE ]; then
            echo "$repo is up-to-date"
        elif [ $LOCAL = $BASE ]; then
            if [ $repo == "/app/irw-client" ]; then
                echo "Stopping React server..."
                kill -INT $react_pid
            fi
            if [ $repo == "/app/irw-server" ]; then
                echo "Stopping Django server..."
                kill -INT $django_pid
                kill -INT $wotpool_pid
                kill -INT $wotminer_pid
            fi
            echo "Pulling changes from origin for $repo"
            # Discard any local changes, then pull from origin
            git checkout .
            git pull origin
            if [ $repo == "/app/irw-client" ]; then
                echo "Starting React server..."
                DISABLE_ESLINT_PLUGIN=true npm --prefix /app/irw-client run start:host &
                react_pid=$!
            fi
            if [ $repo == "/app/irw-server" ]; then
                echo "Running Django model migrations..."
                python3 /app/irw-server/manage.py makemigrations
                python3 /app/irw-server/manage.py migrate
                echo "Starting Django server..."
                python3 /app/irw-server/manage.py runserver 0.0.0.0:8080 &
                django_pid=$!
                python3 /app/irw-server/wotpool/main.py &
                wotpool_pid=$!
                python3 /app/irw-server/toolbox/wot_miner.py &
                wotminer_pid=$!

            fi
        fi
    else
        echo "Directory $repo does not exist. Skipping."
    fi
done

echo "Update repo script run success"
