#!/bin/bash


# Get localhost
localhost="127.0.0.1"

# Get the machine's local IP address
local_ip="$(hostname -I | awk '{print $1}')"

# Get the machine's public IP address
public_ip="$(curl -s ifconfig.me)"

# Get the i2p address from the wot_i2p_address.txt file
i2p_address="$(cat /app/wot_i2p_address.txt)"

# Combine all the addresses
allowed_hosts="$localhost,$local_ip,$public_ip,$i2p_address"

# Update the ALLOWED_HOSTS variable in the .env file without affecting other variables
grep -q '^ALLOWED_HOSTS=' /app/irw-server/irate_wot/.env && sed -i "s/^ALLOWED_HOSTS=.*/ALLOWED_HOSTS=$allowed_hosts/" /app/irw-server/irate_wot/.env || echo "ALLOWED_HOSTS=$allowed_hosts" >> /app/irw-server/irate_wot/.env

# Print the allowed hosts for debugging purposes
echo "Allowed hosts: $allowed_hosts"
sleep 10


echo "Starting post updater..."
python3 /app/irw-server/toolbox/wot-post-updater.py
echo "Starting comment updater..."
python3 /app/irw-server/toolbox/wot-comment-updater.py
echo "Starting vote updater..."
python3 /app/irw-server/toolbox/wot-vote-updater.py
echo "Starting rating updater..."
python3 /app/irw-server/toolbox/wot-rating-updater.py
echo "Starting Git repo updater..."
source /app/irw-s-docker/update_repos.sh # source passes pids to updater

echo "Maintenance round complete. Restarting loop..."
sleep 10
