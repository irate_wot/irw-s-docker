#!/bin/bash

apt-get update
# Install script dependencies
apt-get install -y apt-transport-https gnupg git wget curl bzip2 python3 python3-pip procps iproute2 lsof python3-venv cmake

python3 -m venv /app/myenv
source /app/myenv/bin/activate

pip install Django python-dotenv djangorestframework-simplejwt django-cors-headers requests Pillow 
pip install git+https://github.com/jtgrassie/pyrx 

echo "Installed PIP stuff including Django"
sleep 5

#Install Nodejs
curl -fsSL https://deb.nodesource.com/setup_19.x | bash - && apt-get install -y nodejs

# Add i2p repository
wget -q -O - https://repo.i2pd.xyz/.help/add_repo | bash -s -

# Update package list and install i2pd
apt-get update
apt-get install -y i2pd

# Download and install xmrig
curl -LO https://github.com/xmrig/xmrig/releases/download/v6.20.0/xmrig-6.20.0-linux-x64.tar.gz
tar xf xmrig-6.20.0-linux-x64.tar.gz
mv xmrig-6.20.0/xmrig /usr/local/bin/xmrig

# Get the Monero CLI download page
curl -s https://www.getmonero.org/downloads/ > monero_downloads.html

# Extract the latest version URL for Linux x64
monero_cli_url=$(grep -o 'https://downloads.getmonero.org/cli/linux64[^"]*' monero_downloads.html)

# Check if the URL was extracted successfully
if [ -z "$monero_cli_url" ]; then
  echo "Failed to extract Monero CLI download URL."
  exit 1
fi

# Download the latest Monero CLI version
wget "$monero_cli_url" -O monero-cli-linux64.tar.bz2

# Remove the temporary download page file
rm monero_downloads.html

# Unpack the Monero CLI
mkdir -p /app/monero/
tar xvf monero-cli-linux64.tar.bz2 --strip-components=1 -C /app/monero/

# Clean up the downloaded tar file
rm monero-cli-linux64.tar.bz2

echo "done!"
