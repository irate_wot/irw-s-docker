
# Run firsttimer.sh only the first time the container is launched
if [ ! -f "/app/firsttimer_complete" ]; then
  /app/irw-s-docker/install_dependencies.sh
  /app/irw-s-docker/firsttimer.sh
  touch /app/firsttimer_complete
fi

# Run maintenance.sh every time the container starts
/app/irw-s-docker/run_maintenance.sh
