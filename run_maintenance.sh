#!/bin/bash

# activate virtual environment
source /app/myenv/bin/activate

# Get localhost
localhost="127.0.0.1"

# Get the machine's local IP address
local_ip="$(hostname -I | awk '{print $1}')"

# Get the machine's public IP address
public_ip="$(curl -s ifconfig.me)"

# Get the i2p address from the wot_i2p_address.txt file
i2p_address="$(cat /app/wot_i2p_address.txt)"

# Combine all the addresses
allowed_hosts="$localhost,$local_ip,$public_ip,$i2p_address"

# Update the ALLOWED_HOSTS variable in the .env file without affecting other variables
grep -q '^ALLOWED_HOSTS=' /app/irw-server/irate_wot/.env && sed -i "s/^ALLOWED_HOSTS=.*/ALLOWED_HOSTS=$allowed_hosts/" /app/irw-server/irate_wot/.env || echo "ALLOWED_HOSTS=$allowed_hosts" >> /app/irw-server/irate_wot/.env

# Print the allowed hosts for debugging purposes
echo "Allowed hosts: $allowed_hosts"
sleep 10

# Start I2P
i2pd --daemon --service

# Start UI host
DISABLE_ESLINT_PLUGIN=true npm --prefix /app/irw-client run start:host &
react_pid=$!

# start django backend 
python3 /app/irw-server/manage.py makemigrations
python3 /app/irw-server/manage.py migrate

# This shouldn't be necessary (need to debug why ratings migrations isn't working properly)
python3 /app/irw-server/manage.py makemigrations ratings
python3 /app/irw-server/manage.py migrate ratings

python3 /app/irw-server/manage.py runserver 0.0.0.0:8080 &
django_pid=$!

# Start Monero RPC Node
cd /app/monero/
./monerod --rpc-bind-ip 127.0.0.1 --rpc-bind-port 18081 --restricted-rpc --detach
monero_pid=$!
cd -  # Change back to the original directory

# start WOT pool
python3 /app/irw-server/wotpool/main.py &
wotpool_pid=$!

# start Wot Miner
python3 /app/irw-server/toolbox/wot_miner.py &
wotminer_pid=$!

sleep 60

# Loop 100 times (restart docker container after 100 loops)
for i in {1..100}; do
    # Print the current iteration
    echo "Running iteration $i of 100"
    
    # Run the maintenance.sh script
    source /app/irw-s-docker/maintenance.sh
    
    # Sleep for a while (to prevent tight loop if maintenance.sh exits immediately)
    sleep 10
done

